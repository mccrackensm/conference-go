from django.http import JsonResponse
from .models import Attendee, ConferenceVO, AccountVO
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods

class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]

class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]

class AttendeesDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }
    # def get_extra_data(self, o):
    #     count = AccountVO.objects.filter(email=o.email).count()
    #     print(count)
    #     return {"has_account": count > 0}
        
    def get_extra_data(self, o):
        count = len(AccountVO.objects.filter(email=o.email))
        print("This is your account!!!", count)
        print(o.email)
        if count > 0:
            return {"has_account": True}
        else:
            return {"has_account": False}
        

    # def get_extra_data(self, o):
    #     return { "conference": o.conference}


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            # THIS LINE IS ADDED
            conference_href = f'/api/conferences/{conference_vo_id}/'

            # THIS LINE CHANGES TO ConferenceVO and import_href
            conference = ConferenceVO.objects.get(import_href=conference_href)

            content["conference"] = conference

            ## THIS CHANGES TO ConferenceVO
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeesDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeesDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count = Attendee.objects.filter(id=id).count()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "confernce" in content:
                conference = ConferenceVO.objects.get(id=content["conference"])
                content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Conference"},
                status=400,
            )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeesDetailEncoder,
            safe=False,
        )
