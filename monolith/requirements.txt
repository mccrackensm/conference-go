asgiref==3.6.0
attrs==21.4.0
black==22.1.0
click==8.0.4
colorama==0.4.6
cssbeautifier==1.14.7
Django==4.1.7
djlint==1.19.16
EditorConfig==0.12.3
flake8==4.0.1
html-tag-names==0.1.2
html-void-elements==0.1.0
iniconfig==1.1.1
jsbeautifier==1.14.7
mccabe==0.6.1
mypy-extensions==0.4.3
packaging==21.3
pathspec==0.11.0
platformdirs==2.5.1
pluggy==1.0.0
py==1.11.0
pycodestyle==2.8.0
pyflakes==2.4.0
pyparsing==3.0.7
pytest==7.1.0
PyYAML==6.0
regex==2022.10.31
six==1.16.0
sqlparse==0.4.3
tomli==2.0.1
tqdm==4.64.1
gunicorn==20.1.0
requests==2.28.2
pika==1.2.0